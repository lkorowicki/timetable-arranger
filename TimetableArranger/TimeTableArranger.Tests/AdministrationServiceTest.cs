﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimetableArranger.BLL.Implementation;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.DAL.Entities;
using TimetableArranger.Tests.Mocks;

namespace TimetableArranger.Tests
{
    [TestClass]
    public class AdministrationServiceTest
    {
        IAdministrationService _admSrv;
        ITutorService _tutorSrv;
        
        [TestInitialize]
        public void Initialize()
        {
            _admSrv = new AdministrationService(new UsersRepositoryMock());
            _tutorSrv = new TutorService(new TutorRepositoryMock(), new TitleRepositoryMock());
        }

        [TestMethod]
        public void ValidateUser_properCredentials_returnTrue()
        {
            string login = "jan.kowalski@gmail.com";
            string haslo = "abc123";

            bool result = _admSrv.ValidateUser(login, haslo);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ValidateUser_improperCredentials_returnFalse()
        {
            string nazwisko = "__IMPROPER__";
            string haslo = "__improper__";

            bool result = _admSrv.ValidateUser(nazwisko, haslo);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsUserUnique_duplicateusername_returnFalse()
        {
            string duplicateUsername = _admSrv.GetById(1).mail;

            bool result = _admSrv.IsUsernameUnique(duplicateUsername);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsUserUnique_uniqueUsername_returnTrue()
        {
            string duplicateUsername = "abc_NIE_MA";

            bool result = _admSrv.IsUsernameUnique(duplicateUsername);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetAvailableTutorTitles_returnProperStrings()
        {
            List<string> tytuly = new List<string> {"dr","mgr"};
            List<string> tytulyRepo = _tutorSrv.GetAvailableTutorTitles().Select(t => t.typ).ToList();
            Assert.IsTrue(tytuly.Equals(tytulyRepo));
        }

        [TestCleanup]
        public void Cleanup()
        {
            _admSrv = null;
        }

    }
}
