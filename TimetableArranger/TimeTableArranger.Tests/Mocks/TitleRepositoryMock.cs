﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.Tests.Mocks
{
    public class TitleRepositoryMock : IBaseRepository<TYTULY>
    {
        List<TYTULY> tytuly=new List<TYTULY>();

        public TitleRepositoryMock()
        {
            tytuly.Add(new TYTULY() { typ = "dr", id = 1 });
            tytuly.Add(new TYTULY() { typ = "mgr", id = 2 });
        }

        public bool Add(TYTULY newItem)
        {
            throw new NotImplementedException();
        }

        public TYTULY GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TYTULY> GetAll()
        {
            return tytuly.AsQueryable();
        }

        public bool Remove(TYTULY itemToRemove)
        {
            throw new NotImplementedException();
        }

        public bool Update(TYTULY itemToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
