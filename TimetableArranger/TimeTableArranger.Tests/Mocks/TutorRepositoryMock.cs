﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.Tests.Mocks
{
    public class TutorRepositoryMock : IBaseRepository<PROWADZACY>
    {
        public bool Add(PROWADZACY newItem)
        {
            throw new NotImplementedException();
        }

        public PROWADZACY GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<PROWADZACY> GetAll()
        {
            throw new NotImplementedException();
        }

        public bool Remove(PROWADZACY itemToRemove)
        {
            throw new NotImplementedException();
        }

        public bool Update(PROWADZACY itemToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
