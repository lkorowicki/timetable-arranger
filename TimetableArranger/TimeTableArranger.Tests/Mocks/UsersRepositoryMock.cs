﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.Tests.Mocks
{
    public class UsersRepositoryMock : IBaseRepository<UZYTKOWNIK>
    {
        IList<UZYTKOWNIK> uzytkownicy = new List<UZYTKOWNIK>();

        public UsersRepositoryMock()
        {
            uzytkownicy.Add(new UZYTKOWNIK() { imie = "Jan", nazwisko = "Kowalski", haslo = "abc123", mail = "jan.kowalski@gmail.com", id = 1 });
        }

        public bool Add(UZYTKOWNIK newItem)
        {
            throw new NotImplementedException();
        }

        public UZYTKOWNIK GetById(int id)
        {
            return uzytkownicy.Where(u => u.id == id).FirstOrDefault();
        }

        public IQueryable<UZYTKOWNIK> GetAll()
        {   
            return uzytkownicy.AsQueryable();
        }

        public bool Remove(UZYTKOWNIK itemToRemove)
        {
            throw new NotImplementedException();
        }

        public bool Update(UZYTKOWNIK itemToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
