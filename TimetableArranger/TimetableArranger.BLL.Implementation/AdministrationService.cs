﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.DAL.Interfaces;

namespace TimetableArranger.BLL.Implementation
{
    public class AdministrationService : BaseService<UZYTKOWNIK>, IAdministrationService
    {
        public AdministrationService(IBaseRepository<UZYTKOWNIK> _repository) : base(_repository) { }

        public bool ValidateUser(string username, string password)
        {
            IList<UZYTKOWNIK> lista = repository.GetAll().Where(u => u.mail == username && u.haslo == password).ToList();
            if (lista.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
            //zrobione
        }

        //poprawione 
        public bool IsUsernameUnique(string username)
        {
            IList<UZYTKOWNIK> lista = repository.GetAll().Where(u => u.mail == username).ToList();
            bool unikatowosc = true;
            foreach (var item in lista)
            {
                if (username == item.nazwisko)
                {
                    unikatowosc = false;
                    break;
                }
            }
            return unikatowosc;
        }
    }
}
