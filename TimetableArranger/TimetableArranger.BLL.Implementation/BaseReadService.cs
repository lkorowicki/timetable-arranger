﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.DAL.Interfaces;

namespace TimetableArranger.BLL.Implementation
{
    //gotowe
    public class BaseReadService<T> : IBaseReadService<T> where T:class
    {
        protected IBaseRepository<T> repository;

        public BaseReadService(IBaseRepository<T> _repository)
        {
            this.repository = _repository;
        }
                
        public T GetById(int id)
        {
            return repository.GetById(id);
        }

        public IList<T> GetAll()
        {
            return repository.GetAll().ToList();
        }
    }
}
