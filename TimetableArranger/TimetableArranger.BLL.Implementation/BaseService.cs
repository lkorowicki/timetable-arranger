﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.BLL.Interfaces;

namespace TimetableArranger.BLL.Implementation
{
    //gotowa
    public class BaseService<T> : BaseReadService<T>, IBaseService<T> where T:class 
    {
        public BaseService(IBaseRepository<T> _repository) : base(_repository) { }
        
        public bool Add(T newItem)
        {
            return repository.Add(newItem);
        }

        public bool Remove(T itemToRemove)
        {
            return repository.Remove(itemToRemove);
        }

        public bool Update(T itemToUpdate)
        {
            return repository.Update(itemToUpdate);
        }
    }
}
