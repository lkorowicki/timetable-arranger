﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.BLL.Interfaces;

namespace TimetableArranger.BLL.Implementation
{
    public class ClassroomService : BaseService<SALE>, IClassroomService
    {
        private IBaseRepository<TYP_SAL> _classroomTypesRepository;

        public ClassroomService(IBaseRepository<SALE> _repository, IBaseRepository<TYP_SAL> _classroomTypesRepository) : base(_repository) 
        {
            this._classroomTypesRepository = _classroomTypesRepository;
        }
        //ale o co cho? zrobic pozniej
    
        public IList<TYP_SAL> GetClassroomTypes()
        {
            return _classroomTypesRepository.GetAll().ToList();
        }
    }
}
