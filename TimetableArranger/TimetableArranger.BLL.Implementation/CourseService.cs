﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.BLL.Interfaces;

namespace TimetableArranger.BLL.Implementation
{
    public class CourseService : BaseService<ZAJECIA>, ICourseService
    {
        IBaseRepository<PRZEDMIOT> subjectRepository;
        IBaseRepository<TYP_SAL> typesRepository;

        public CourseService(IBaseRepository<ZAJECIA> _repository, IBaseRepository<PRZEDMIOT> _subjectRepository, IBaseRepository<TYP_SAL> _typesRepository) : base(_repository) 
        {
            subjectRepository = _subjectRepository;
            typesRepository = _typesRepository;
        }
        

        public bool AddSubjectAndCourse(PRZEDMIOT subject, ZAJECIA course)
        {
            bool result = subjectRepository.Add(subject);
            if (!result) return false;
            int subjectId = subjectRepository.GetAll().Where(s => s.nazwa == subject.nazwa && s.semestr == subject.semestr).Single().id;
            course.przedmiot = subjectId;
            result &= repository.Add(course);

            return result;
        }


        public IQueryable<TYP_SAL> GetCourseTypes()
        {
            return typesRepository.GetAll();
        }
    }
}
