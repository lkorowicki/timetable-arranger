﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.DAL.Entities;
using TimetableArranger.DAL.Interfaces;

namespace TimetableArranger.BLL.Implementation
{
    public class GroupService : BaseService<GRUPA>,IGroupService
    {
        public GroupService(IBaseRepository<GRUPA> _repository) : base(_repository) { }
    }
}
