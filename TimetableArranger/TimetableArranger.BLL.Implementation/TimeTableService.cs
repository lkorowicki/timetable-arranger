﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.BLL.Implementation
{
    public class TimeTableService : ITimeTableArrangerService
    {
        private IBaseRepository<PROWADZACY> tutorRepository;
        private IBaseRepository<GRUPA> groupRepository;
        private ICourseService courseService;
        private IBaseRepository<ZAJECIA> lessonsRepository;

        public TimeTableService(IBaseRepository<PROWADZACY> _tutorRespoitory, IBaseRepository<GRUPA> _groupRepository, ICourseService _courseService, IBaseRepository<ZAJECIA> _lessonsRepository)
        {
            this.tutorRepository = _tutorRespoitory;
            this.groupRepository = _groupRepository;
            this.courseService = _courseService;
            this.lessonsRepository = _lessonsRepository;

        }
        /*
        public IList<ZAJECIA> GetUnassignedCourseItems(TimeSpan dlugosc, int dzien, TimeSpan godzina)
        {
            return lessonsRepository.GetAll().Where(u => u.dlugosc == dlugosc && u.dzien==dzien && u.godzina==godzina).ToList();
        }*/

        public IList<ZAJECIA> GetUnassignedCourseItems()
        {
            return lessonsRepository.GetAll().Where(u => u.dzien == 0 || u.godzina == TimeSpan.Zero).ToList();
        }

        public IList<ZAJECIA> GetCourseItemsForTutor(PROWADZACY tutor)
        {
            return courseService.GetAll().Where(u => u.PROWADZACY1 == tutor).ToList();
        }

        public IList<ZAJECIA> GetCourseItemsForGroup(GRUPA group)
        {
            return courseService.GetAll().Where(u => u.GRUPA1 == group).ToList();
        }
    }
}
