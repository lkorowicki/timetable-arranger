﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.BLL.Interfaces;

namespace TimetableArranger.BLL.Implementation
{
    public class TutorService : BaseService<PROWADZACY>, ITutorService
    {
        IBaseRepository<TYTULY> _titleRepository;
        
        public TutorService(IBaseRepository<PROWADZACY> _repository,IBaseRepository<TYTULY> _titleRepository) : base(_repository) 
        {
            this._titleRepository = _titleRepository;
        }
        //ale o co cho? zrobic pozniej

        //potrzeba przekazać o ktorego prowadzącego chodzi - chyba, ze sprawdzamy listę
        //prowadzących i patrzymy, którzy danego dnia i o danej godzinie są wolni - 
        //to wtedy zwracamy listę a nie bool
        public bool CheckTutorAvailability(int dayOfWeek, TimeSpan timeToCheck)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Pobiera nazwy tytułów
        /// </summary>
        /// <returns></returns>
        /// 
        //o to chodzi? czy te tytuły mają być tylko takie, jakie posiadają dostępni prowadzący?
        public List<TYTULY> GetAvailableTutorTitles()
        {
            return _titleRepository.GetAll().ToList();

            //ZAŚLEPKA
            //return new List<TYTULY>();
            //ZAŚLEPKA
        }
    }
}
