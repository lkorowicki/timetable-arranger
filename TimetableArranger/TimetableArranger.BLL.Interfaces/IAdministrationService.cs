﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.BLL.Interfaces
{
    public interface IAdministrationService : IBaseService<UZYTKOWNIK>
    {
        bool ValidateUser(string username, string password);
        bool IsUsernameUnique(string username);
    }
}
