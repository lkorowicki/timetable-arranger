﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimetableArranger.BLL.Interfaces
{
    public interface IBaseReadService<T> where T:class
    {
        T GetById(int id);
        IList<T> GetAll();
    }
}
