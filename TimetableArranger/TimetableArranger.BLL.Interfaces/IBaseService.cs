﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimetableArranger.BLL.Interfaces
{
    public interface IBaseService<T> : IBaseReadService<T> where T:class
    {
        bool Add(T newItem);        
        bool Remove(T itemToRemove);
        bool Update(T itemToUpdate);
    }
}
