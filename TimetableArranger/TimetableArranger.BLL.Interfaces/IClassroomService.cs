﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.BLL.Interfaces
{
    public interface IClassroomService : IBaseService<SALE>
    {
        IList<TYP_SAL> GetClassroomTypes();
    }
}
