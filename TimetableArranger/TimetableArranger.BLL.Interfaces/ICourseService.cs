﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.BLL.Interfaces
{
    public interface ICourseService : IBaseService<ZAJECIA>
    {
        bool AddSubjectAndCourse(PRZEDMIOT subject, ZAJECIA course);
        IQueryable<TYP_SAL> GetCourseTypes();
    }
}
