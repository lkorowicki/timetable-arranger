﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.BLL.Interfaces
{
    public interface ITimeTableArrangerService
    {
        IList<ZAJECIA> GetUnassignedCourseItems();
        IList<ZAJECIA> GetCourseItemsForTutor(PROWADZACY tutor);
        IList<ZAJECIA> GetCourseItemsForGroup(GRUPA group);

    }
}
