﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.BLL.Interfaces
{
    public interface ITutorService : IBaseService<PROWADZACY>
    {
        bool CheckTutorAvailability(int dayOfWeek, TimeSpan timeToCheck);
        List<TYTULY> GetAvailableTutorTitles();
    }
}
