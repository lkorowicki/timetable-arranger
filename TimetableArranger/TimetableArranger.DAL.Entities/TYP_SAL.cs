//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TimetableArranger.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class TYP_SAL
    {
        public TYP_SAL()
        {
            this.SALE = new HashSet<SALE>();
            this.ZAJECIA = new HashSet<ZAJECIA>();
        }
    
        public int id { get; set; }
        public string typ { get; set; }
    
        public virtual ICollection<SALE> SALE { get; set; }
        public virtual ICollection<ZAJECIA> ZAJECIA { get; set; }
    }
}
