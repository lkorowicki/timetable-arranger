﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.DAL.Entities;
using System.Data.Entity;

namespace TimetableArranger.DAL.Implementation
{
    public class BaseRepository<T> : IBaseRepository<T> where T:class
    {
        private zespolowyEntities context;

        private IDbSet<T> TEntities { get { return context.Set<T>(); } }

        public BaseRepository(zespolowyEntities _context)
        {
            this.context = _context;
        }

        public BaseRepository() : this(new zespolowyEntities()) { }

        public bool Add(T newItem)
        {
            TEntities.Add(newItem);
            return context.SaveChanges() > 0;
        }

        public T GetById(int id)
        {
            return TEntities.Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return TEntities.AsQueryable();
        }

        public bool Remove(T itemToRemove)
        {
            TEntities.Remove(itemToRemove);
            return context.SaveChanges() > 0;
        }

        public bool Update(T itemToUpdate)
        {
            if (itemToUpdate != null)
                return context.SaveChanges() > 0;

            return false;
        }
    }
}
