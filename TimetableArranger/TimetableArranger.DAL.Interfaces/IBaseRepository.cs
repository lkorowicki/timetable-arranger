﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimetableArranger.DAL.Interfaces
{
    public interface IBaseRepository<T> where T:class
    {
        bool Add(T newItem);
        T GetById(int id);
        IQueryable<T> GetAll();
        bool Remove(T itemToRemove);
        bool Update(T itemToUpdate);
    }
}
