﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimetableArranger.Web.Models;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.Web.Infrastructure;
using TimetableArranger.DAL.Entities;
using System.Web.Security;

namespace TimetableArranger.Web.Controllers
{
    public class AccountController : Controller
    {
        IAdministrationService admService;

        public AccountController(IAdministrationService _admService)
        {
            this.admService = _admService;
        }

        /// <summary>
        /// Wyświetlenie ekranu logowania
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Przetworzenie requestu logowania
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(LoginModel model)
        {              
            if (ModelState.IsValid)
            {
                if (admService.ValidateUser(model.Login, model.Password))
                {
                    //Utowrzenie ciasteczka, żeby zapamiętać, że user jest zalogowany
                        //Response.Cookies.Add(CookieHelper.CreateCookie(model.Login));                    
                    CookieHelper.CreateCookie(model.Login);

                    return RedirectToAction("Create","Tutor");
                }
                else
                {
                    ModelState.AddModelError("", StaticStrings.ModelErrorMsgs.LoginFail);
                }
            }
            return View(model);
        }

        /// <summary>
        /// Wyświetlenie ekranu rejestracji
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// Przetworzenie requestu rejestracji
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                if (admService.Add(model.ParseToEntity()))
                    return RedirectToAction("Create", "Tutor");
                else
                    ModelState.AddModelError("",StaticStrings.ModelErrorMsgs.RegisterFail);
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            CookieHelper.Logout();
            return RedirectToAction("Login");
        }

    }
}
