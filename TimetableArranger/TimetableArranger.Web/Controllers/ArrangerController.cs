﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.Web.Models;
using System.Web.Helpers;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.Web.Controllers
{
    public class ArrangerController : Controller
    {
        //
        // GET: /Arranger/
        ITimeTableArrangerService timetableSrv;
        ICourseService courseService;

        public ArrangerController(ITimeTableArrangerService _timetableSrv, ICourseService _courseService)
        {
            this.timetableSrv = _timetableSrv;
            this.courseService = _courseService;
        }

        [Authorize]
        public ActionResult Index()
        {
            ArrangerModel model = new ArrangerModel();
            model.Load(timetableSrv.GetUnassignedCourseItems());
            return View("My",model);
        }

        [Authorize]
        public JsonResult SaveCourse(int courseid, int day, TimeSpan time)
        {
            ZAJECIA course = courseService.GetById(courseid);
            if(course!=null)
            {
                course.godzina = time;
                course.dzien = day;
                courseService.Update(course);
            }
            return Json(true,JsonRequestBehavior.AllowGet);
        }

    }
}
