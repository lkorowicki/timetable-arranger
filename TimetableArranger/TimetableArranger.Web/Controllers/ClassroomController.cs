﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.Web.Models;
using TimetableArranger.Web.Infrastructure;

namespace TimetableArranger.Web.Controllers
{
    public class ClassroomController : Controller
    {
        
        IClassroomService classService;
        IBuildingService buildingService;

        public ClassroomController(IClassroomService _classService, IBuildingService _buildingService)
        {
            this.classService = _classService;
            this.buildingService = _buildingService;
        }

        //
        // GET: /Classroom/
        /// <summary>
        /// Wyświetlenie formularza dodawania sali
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Create()
        {
            ClassroomModel model = new ClassroomModel();
            model.Load(buildingService.GetAll(), classService.GetAll(), classService.GetClassroomTypes());
            return View(model);
        }

        /// <summary>
        /// Obsługa requestu dodania sali
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(ClassroomModel model)
        {
            if (ModelState.IsValid)
            {
                if (classService.Add(model.ParseToEntity()))
                    return RedirectToAction("Create", "Course");
                else
                    ModelState.AddModelError("", StaticStrings.ModelErrorMsgs.ClassroomSaveFail);
            }
            model.Load(buildingService.GetAll(), classService.GetAll(), classService.GetClassroomTypes());
            return View(model);
        }

    }
}
