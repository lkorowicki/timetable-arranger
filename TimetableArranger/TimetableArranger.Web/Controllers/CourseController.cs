﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimetableArranger.Web.Models;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.Web.Infrastructure;

namespace TimetableArranger.Web.Controllers
{
    public class CourseController : Controller
    {
        
        ICourseService courseService;
        ITutorService tutorService;

        public CourseController(ICourseService _courseService,ITutorService _tutorService)
        {
            this.courseService = _courseService;
            this.tutorService = _tutorService;
        }

        //
        // GET: /Course/
        [Authorize]
        public ActionResult Create()
        {
            CourseModel model = new CourseModel();
            model.Load(tutorService.GetAll().ToList(), courseService.GetAll().ToList(),courseService.GetCourseTypes().ToList());
            return View(model);
        }

        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(CourseModel model)
        {
            if (ModelState.IsValid)
            {
                if (courseService.AddSubjectAndCourse(model.GiveSubject(),model.GiveCourse()))
                    return RedirectToAction("Create", "Course");
                else
                    ModelState.AddModelError("", StaticStrings.ModelErrorMsgs.ClassroomSaveFail);
            }
            return View(model);
        }

    }
}
