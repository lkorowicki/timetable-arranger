﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.Web.Models;
using TimetableArranger.Web.Infrastructure;

namespace TimetableArranger.Web.Controllers
{
    public class GroupController : Controller
    {
        //
        // GET: /Group/
        IGroupService groupService;

        public GroupController(IGroupService _groupService)
        {
            this.groupService = _groupService;
        }

        /// <summary>
        /// Wyświetlenie formularza dodawania grupy
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Create()
        {
            GroupModel model = new GroupModel();
            model.Load(groupService.GetAll().ToList());
            return View(model);
        }

        /// <summary>
        /// Obsługa requestu dodania grupy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(GroupModel model)
        {
            if (ModelState.IsValid)
            {
                if (groupService.Add(model.ParseToEntity()))
                    return RedirectToAction("Index", "Arranger");
                else
                    ModelState.AddModelError("", StaticStrings.ModelErrorMsgs.GroupSaveFail);
            }
            return View(model);
        }
    }
}
