﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.Web.Models;
using TimetableArranger.DAL.Entities;
using TimetableArranger.Web.Infrastructure;

namespace TimetableArranger.Web.Controllers
{
    public class TutorController : Controller
    {
        //
        // GET: /Tutor/

        ITutorService tutorService;

        public TutorController(ITutorService _tutorService)
        {
            this.tutorService = _tutorService;
        }

        /// <summary>
        /// Wyświetlenie formularza dodawania nowego prowadzącego
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Create()
        {
            TutorCreateModel model = new TutorCreateModel();
            model.Load(tutorService.GetAvailableTutorTitles(), tutorService.GetAll().ToList());
            return View(model);
        }

        /// <summary>
        /// Obsługa requestu dodania prowadzącego
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(TutorCreateModel model)
        {
            if (ModelState.IsValid)
            {
                if(tutorService.Add(model.ParseToEntity()))
                    return RedirectToAction("Create","Classroom");
                else
                    ModelState.AddModelError("",StaticStrings.ModelErrorMsgs.TutorSaveFail);
            }
            model.Load(tutorService.GetAvailableTutorTitles(), tutorService.GetAll().ToList());
            return View(model);
        }

    }
}
