﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace TimetableArranger.Web.Infrastructure
{
    public static class CookieHelper
    {
        public static bool IsLogged()
        {
            return HttpContext.Current.Request.IsAuthenticated;
        }

        /*public static HttpCookie CreateCookie(string username)
        {
            FormsAuthenticationTicket ticket = new
                        FormsAuthenticationTicket(1,
                        username,
                        DateTime.Now,
                        DateTime.Now.AddYears(1),
                        true,
                        "",
                        FormsAuthentication.FormsCookiePath);

            HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
            if (ticket.IsPersistent) authCookie.Expires = ticket.Expiration;
            return authCookie;
        }*/

        public static void CreateCookie(string username)
        {
            FormsAuthentication.SetAuthCookie(username, true);
        }

        public static string GetUsername()
        {
            return HttpContext.Current.User.Identity.Name;
        }

        public static void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}