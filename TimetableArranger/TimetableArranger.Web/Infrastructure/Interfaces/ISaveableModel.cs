﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.Web.Infrastructure.Interfaces
{
    public interface ISaveableModel<T> where T:class
    {
        T ParseToEntity();
    }
}
