﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Web.Mvc;
using TimetableArranger.BLL.Interfaces;
using TimetableArranger.BLL.Implementation;
using TimetableArranger.DAL.Entities;
using TimetableArranger.DAL.Interfaces;
using TimetableArranger.DAL.Implementation;

namespace TimetableArranger.Web.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)kernel.Get(controllerType);
        }

        private void AddBindings()
        {
            kernel.Bind<IAdministrationService>().To<AdministrationService>();
            kernel.Bind<IBaseRepository<UZYTKOWNIK>>().To<BaseRepository<UZYTKOWNIK>>();
            kernel.Bind<ITutorService>().To<TutorService>();
            kernel.Bind<IBaseRepository<PROWADZACY>>().To<BaseRepository<PROWADZACY>>();
            kernel.Bind<IBaseRepository<TYTULY>>().To<BaseRepository<TYTULY>>();
            kernel.Bind<IGroupService>().To<GroupService>();
            kernel.Bind<ICourseService>().To<CourseService>();
            kernel.Bind<IBaseRepository<GRUPA>>().To<BaseRepository<GRUPA>>();
            kernel.Bind<IBaseRepository<SALE>>().To<BaseRepository<SALE>>();
            kernel.Bind<IBaseRepository<TYP_SAL>>().To<BaseRepository<TYP_SAL>>();
            kernel.Bind<IBaseRepository<ZAJECIA>>().To<BaseRepository<ZAJECIA>>();
            kernel.Bind<IBaseRepository<BUDYNKI>>().To<BaseRepository<BUDYNKI>>();
            kernel.Bind<IBuildingService>().To<BuildingService>();
            kernel.Bind<ITimeTableArrangerService>().To<TimeTableService>();
            kernel.Bind<IClassroomService>().To<ClassroomService>();
            kernel.Bind<IBaseRepository<PRZEDMIOT>>().To<BaseRepository<PRZEDMIOT>>();
        }
    }
}