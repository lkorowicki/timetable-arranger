﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimetableArranger.Web.Infrastructure
{
    public static class StaticStrings
    {
        public static class ModelErrorMsgs
        {
            public static readonly string LoginFail
                = "Niepoprawny login i/lub hasło.";
            public static readonly string RegisterFail
                = "Nie udało się zapisać nowego użytkownika do bazy.";
            public static readonly string TutorSaveFail
                = "Nie udało się zapisać prowadzącego w bazie.";
            public static readonly string ClassroomSaveFail
                = "Nie udało się zapisać sali w bazie.";
            public static readonly string GroupSaveFail
                = "Nie udało się zapisać grupy w bazie.";
            public static readonly string CourseSaveFail
                = "Nie udało się zapisać przedmiotu.";
        }
    }
}