﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.Web.Models
{
    public class ArrangerModel
    {
        public List<string> AvailableHours { get; set; }
        public int GroupCount { get; set; }
        public List<string> WeekDays = new List<string> { "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek" };
        public List<UnassignedCoursesViewModel> UnassignedCourses;
        
        public void Load(IList<ZAJECIA> unassignedCourses)
        {
            AvailableHours = new List<string>();
            for (int i = 8; i < 21; i++)
                AvailableHours.Add(string.Format("{0}:00", i));

            GroupCount = 8;
            UnassignedCourses = unassignedCourses.Select(c => new UnassignedCoursesViewModel()
            {
                Id = c.id,
                Name = c.PRZEDMIOT1.nazwa
            }).ToList();
        }

        public class UnassignedCoursesViewModel
        {
            public int Id {get;set;}
            public string Name {get;set;}
        }
    }
}