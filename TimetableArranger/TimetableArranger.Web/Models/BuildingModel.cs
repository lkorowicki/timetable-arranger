﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using TimetableArranger.DAL.Entities;

namespace TimetableArranger.Web.Models
{
    public class BuildingModel
    {
        [Required(ErrorMessage = "Musisz podać numer budynku")]
        public string Number { get; set; }

        [Required(ErrorMessage = "Musisz podać wydział")]
        public string Faculty { get; set; }
    }
}