﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using TimetableArranger.DAL.Entities;
using TimetableArranger.Web.Infrastructure.Interfaces;

namespace TimetableArranger.Web.Models
{
    public class ClassroomModel : ISaveableModel<SALE>
    {
        
        //NIE MA OBSŁUGI ZASOBÓW, BO NIE MA ICH BAZIE!!!

        public ClassroomViewModel thisClassroom {get;set;}
       

        public IList<ClassroomViewModel> ClassroomList { get; set; }
        
        public List<SelectListItem> BuildingsList { get; set; }

        [Required(ErrorMessage = "Musisz wybrać typ sali")]
        public int TypeId { get; set; }
        public List<SelectListItem> TypesList { get; set; }
        
        public class ClassroomViewModel
        {
            [Required(ErrorMessage = "Musisz podać numer sali")]
            public string Number { get; set; }
            [Required(ErrorMessage = "Musisz podać piętro")]
            public int Floor { get; set; }
            [Required(ErrorMessage = "Musisz wybrać numer budynku")]
            public int BuildingNo { get; set; }
            public string TypeText { get; set; }
            [Required(ErrorMessage="Musisz podać liczbę stanowisk")]
            public int SeatsNumber { get; set; }
        }

        #region methods
        public void Load(IList<BUDYNKI> buildingList, IList<SALE> classroomList, IList<TYP_SAL> classroomTypesList)
        {
            BuildingsList = buildingList.Select(n => new SelectListItem()
            {
                Text = n.nr+" "+n.wydzial,
                Value = n.id.ToString()
            }).ToList();


            ClassroomList = classroomList.Select(q => new ClassroomViewModel()
            {
                Number = q.nr,
                Floor = q.pietro,
                BuildingNo = q.BUDYNKI.nr,
                TypeText = q.TYP_SAL.typ,
                SeatsNumber = q.ilosc_stanowisk
            }).ToList();

            TypesList = classroomTypesList.Select(q => new SelectListItem()
            {
                Text = q.typ,
                Value = q.id.ToString()
            }).ToList();
        }

        public SALE ParseToEntity()
        {
            return new SALE()
            {
                id_bud = this.thisClassroom.BuildingNo,
                nr = this.thisClassroom.Number,
                pietro = this.thisClassroom.Floor,
                typ = this.TypeId,
                ilosc_stanowisk = this.thisClassroom.SeatsNumber
            };
        }
        #endregion
    

}
}