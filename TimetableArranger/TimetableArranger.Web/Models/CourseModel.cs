﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimetableArranger.DAL.Entities;
using TimetableArranger.Web.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace TimetableArranger.Web.Models
{
    public class CourseModel //: ISaveableModel<ZAJECIA>
    {
        public List<SelectListItem> TutorsList { get; set; }
        [Required(ErrorMessage="Musisz wybrać prowadzącego")]
        public int TutorId { get; set; }
        public IList<CourseViewModel> CoursesList { get; set; }
        public List<SelectListItem> ClassroomTypesList { get; set; }
        public CourseViewModel thisCourse { get; set; }
        [Required(ErrorMessage="Musisz wybrać rodzaj zajęć.")]
        public int TypeId { get; set; }

        public CourseModel()
        {
            TutorsList = new List<SelectListItem>();
            CoursesList = new List<CourseViewModel>();
        }

        #region methods
        public void Load(IList<PROWADZACY> _tutorsList,IList<ZAJECIA> _coursesList,IList<TYP_SAL> _classroomTypesList)
        {
            TutorsList = _tutorsList.Select(i => new SelectListItem() { Text = i.nazwisko, Value = i.id.ToString() }).ToList();
            CoursesList = new List<CourseViewModel>();
            foreach(var course in _coursesList)
            {
                var cvm = new CourseViewModel();
                cvm.CourseName = course.PRZEDMIOT1.nazwa;
                cvm.TutorName = course.PROWADZACY1.imie + " " + course.PROWADZACY1.nazwisko;
                cvm.Duration = course.dlugosc;
                cvm.Semester = course.PRZEDMIOT1.semestr.ToString();
                cvm.Type = course.TYP_SAL.typ;
                CoursesList.Add(cvm);
            }
            ClassroomTypesList = _classroomTypesList.Select(t => new SelectListItem()
            {
                Text = t.typ,
                Value = t.id.ToString()
            }).ToList();
        }

        public ZAJECIA GiveCourse()
        {
            return new ZAJECIA()
            {
                dlugosc = this.thisCourse.Duration,
                prowadzacy = this.TutorId,
                typ = this.TypeId
            };
        }
        public PRZEDMIOT GiveSubject()
        {
            return new PRZEDMIOT()
            {
                nazwa = this.thisCourse.CourseName,
                semestr = Int32.Parse(this.thisCourse.Semester)                
            };
        }

        #endregion

        public class CourseViewModel
        {
            [Required(ErrorMessage="Musisz podać nazwę przedmiotu.")]
            public string CourseName { get; set; }
            public string TutorName { get; set; }
            [Required(ErrorMessage="Musisz podać długość trwania zajęć.")]
            [DisplayFormat(DataFormatString="hh:mm")]
            public TimeSpan Duration { get; set; }
            public string Semester { get; set; }
            public string Type { get; set; }
        }
    }

}