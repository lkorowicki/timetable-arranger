﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimetableArranger.DAL.Entities;
using TimetableArranger.Web.Infrastructure.Interfaces;

namespace TimetableArranger.Web.Models
{
    public class GroupModel : ISaveableModel<GRUPA>
    {
        public GroupViewModel GroupEdited;

        public List<GroupViewModel> GroupsList { get; set; }

        public GroupModel()
        {
            GroupsList = new List<GroupViewModel>();
        }

        public class GroupViewModel
        {
            public string Name { get; set; }
            public string AbbreviatedName { get; set; }
            public int Semester { get; set; }
            public int StudentsCount { get; set; }
        }

        #region methods 

        public GRUPA ParseToEntity()
        {
            return new GRUPA() 
            { 
                ilosc_osob = this.GroupEdited.StudentsCount,
                nazwa = this.GroupEdited.Name,
                semestr = this.GroupEdited.Semester                
            };
        }

        public void Load(List<GRUPA> _groupList)
        {
            foreach (var group in _groupList)
            {
                GroupsList.Add(new GroupViewModel()
                {
                    Name = group.nazwa,
                    AbbreviatedName = group.nazwa, //w bazie brakuje nazwy skróconej!!!
                    Semester = 0, //group.semestr, //wywal nullowalność tej kolumny
                    StudentsCount = 0// group.ilosc_osob //wywal nullowalność tej kolumny
                });
            }
        }

        #endregion 
    }
}