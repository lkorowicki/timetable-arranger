﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using TimetableArranger.DAL.Entities;
using TimetableArranger.Web.Infrastructure.Interfaces;

namespace TimetableArranger.Web.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Musisz podać nazwę użytkownika")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Musisz podać hasło")]
        public string Password { get; set; }
    }

    public class RegisterModel : ISaveableModel<UZYTKOWNIK>
    {
        [Required(ErrorMessage = "Musisz podać nazwę użytkownika")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Musisz podać hasło")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Musisz podać adres e-mail")]
        [RegularExpression(@"^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$", ErrorMessage = "Podałeś e-mail w niepoprawnym formacie")]
        public string EMail { get; set; }

        [Required(ErrorMessage = "Musisz wpisać hasło dwukrotnie")]
        [CompareAttribute("Password", ErrorMessage = "Podane hasła muszą być takie same w obu polach")]
        public string ConfirmPassword { get; set; }

        #region methods

        public UZYTKOWNIK ParseToEntity()
        {
            return new UZYTKOWNIK()
            {
                imie = "", //baza do poprawki
                nazwisko = "", //baza do poprawki
                haslo = this.Password,
                mail = this.EMail //,
                //uprawnienia = 
            };
                    
        }
        #endregion
    }

}