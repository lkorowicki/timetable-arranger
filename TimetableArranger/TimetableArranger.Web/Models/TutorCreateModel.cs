﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using TimetableArranger.DAL.Entities;
using TimetableArranger.Web.Infrastructure.Interfaces;

namespace TimetableArranger.Web.Models
{
    public class TutorCreateModel : ISaveableModel<PROWADZACY>
    {
        [Required(ErrorMessage = "Musisz podać imię prowadzącego")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Musisz podać nazwisko prowadzącego")]
        public string Surname { get; set; }

        [Required(ErrorMessage="Musisz wybrać tytuł naukowy prowadzącego")]
        public int TutorTitleId { get; set; }

        [Required(ErrorMessage="Musisz podać e-mail prowadzącego")]
        public string Email { get; set; }

        public IEnumerable<SelectListItem> Titles { get; set; }

        public IList<TutorViewModel> TutorsList { get; set; }

        public TutorCreateModel()
        {
            Titles = new List<SelectListItem>();
        }

        #region methods

        public void Load(IList<TYTULY> titlesList, IList<PROWADZACY> tutorsList)
        {

            Titles = titlesList.Select(q => new SelectListItem()
            {
                Text = q.typ,
                Value = q.id.ToString()
            });

            TutorsList = tutorsList.Select(q => new TutorViewModel() { Name = string.Format("{0} {1}", q.imie, q.nazwisko), Email = q.mail }).ToList();
                        
        }

        public PROWADZACY ParseToEntity()
        {
            return new PROWADZACY()
            {
                haslo = "", // poprawka do bazy albo random
                imie = this.FirstName,
                nazwisko = this.Surname,
                mail = this.Email,
                tytul = TutorTitleId
            };
        }

        #endregion

        public class TutorViewModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
        }
    }
}