USE [zespolowy]
GO
SET IDENTITY_INSERT [dbo].[TYPY_SAL] ON 

INSERT [dbo].[TYPY_SAL] ([id], [typ]) VALUES (1, N'ćw')
INSERT [dbo].[TYPY_SAL] ([id], [typ]) VALUES (2, N'lab')
INSERT [dbo].[TYPY_SAL] ([id], [typ]) VALUES (3, N'wyk')
INSERT [dbo].[TYPY_SAL] ([id], [typ]) VALUES (4, N'elek')
INSERT [dbo].[TYPY_SAL] ([id], [typ]) VALUES (5, N'aula')
SET IDENTITY_INSERT [dbo].[TYPY_SAL] OFF
SET IDENTITY_INSERT [dbo].[TYTULY] ON 

INSERT [dbo].[TYTULY] ([id], [typ]) VALUES (1, N'mgr')
INSERT [dbo].[TYTULY] ([id], [typ]) VALUES (2, N'mgr inż.')
INSERT [dbo].[TYTULY] ([id], [typ]) VALUES (3, N'dr')
INSERT [dbo].[TYTULY] ([id], [typ]) VALUES (4, N'dr inż.')
INSERT [dbo].[TYTULY] ([id], [typ]) VALUES (5, N'dr hab.')
INSERT [dbo].[TYTULY] ([id], [typ]) VALUES (6, N'prof. dr hab.')
SET IDENTITY_INSERT [dbo].[TYTULY] OFF
