﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL.Interfaces
{
    public interface IShopService
    {
        IList<Produkty> GetProduktyStartsWith(char letter);
        IList<Produkty> GetProduktyQuota(int howMany);
        IList<Dzialy> GetDzialy();
        int GetDzialyCountProducts(string name);
    }
}
