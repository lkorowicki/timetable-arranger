﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Repositories.Interfaces;
using DAL;
using BLL.Interfaces;

namespace BLL
{
    public class ShopService : IShopService
    {
        IRepository<Produkty> produktyRepository;
        IRepository<Dzialy> dzialyRepository;

        public ShopService(IRepository<Produkty> _produktyRepository, IRepository<Dzialy> _dzialyRepository)
        {
            this.produktyRepository = _produktyRepository;
            this.dzialyRepository = _dzialyRepository;
        }

        public IList<Produkty> GetProduktyStartsWith(char letter)
        {
            return produktyRepository.GetAll().Where(p => p.Nazwa.ToLower().StartsWith(letter.ToString())).ToList();
        }

        public IList<Produkty> GetProduktyQuota(int howMany)
        {
            return produktyRepository.GetAll().Take(howMany).ToList();
        }

        public IList<Dzialy> GetDzialy()
        {
            return dzialyRepository.GetAll().ToList();
        }

        public int GetDzialyCountProducts(string name)
        {
            return produktyRepository.GetAll().Where(p => p.Dzialy.Select(d => d.Nazwa).Contains(name)).Count();
        }
    }
}
