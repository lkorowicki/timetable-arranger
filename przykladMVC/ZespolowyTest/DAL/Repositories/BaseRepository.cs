﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Repositories.Interfaces;
using System.Data.Entity;

namespace DAL.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T:class
    {

        private Entities context;
        private IDbSet<T> TEntities { get { return context.Set<T>(); } }

        public BaseRepository(Entities _context)
        {
            this.context = _context;
        }

        public BaseRepository() : this(new Entities()) { }


        public T GetById(int id)
        {
            return TEntities.Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return TEntities.AsQueryable();
        }

        public void Insert(T obj)
        {
            TEntities.Add(obj);
            context.SaveChanges();
        }

        public void Update(T obj)
        {
            if (obj != null)
                context.SaveChanges();
        }

        public void Delete(T obj)
        {
            TEntities.Remove(obj);

        }
    }
}
