﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public interface IRepository<T> where T:class
    {
        T GetById(int id);
        IQueryable<T> GetAll();
        void Insert(T obj);
        void Update(T obj);
        void Delete(T obj);
    }
}
