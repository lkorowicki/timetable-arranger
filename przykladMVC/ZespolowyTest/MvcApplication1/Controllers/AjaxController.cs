﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.Interfaces;

namespace MvcApplication1.Controllers
{
    public class AjaxController : Controller
    {
        IShopService shopService;

        public AjaxController(IShopService _shopService)
        {
            this.shopService = _shopService;
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult WywolanieAjaksowe(string parametr)
        {
            int liczbaProduktow = shopService.GetDzialyCountProducts(parametr);
            
            return new JsonResult(){Data=new {poleZwrocone=liczbaProduktow},JsonRequestBehavior=JsonRequestBehavior.AllowGet};
        }

    }
}
