﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;
using BLL.Interfaces;

namespace MvcApplication1.Controllers
{
    public class DzialyController : Controller
    {
        //
        // GET: /Dzialy/
        IShopService shopService;

        public DzialyController(IShopService _shopService)
        {
            this.shopService = _shopService;
        }
        
        public ActionResult Index()
        {
            DzialyModel model = new DzialyModel(shopService.GetDzialy());
            return View(model);
        }

    }
}
