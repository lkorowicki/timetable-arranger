﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using BLL.Interfaces;
using BLL;
using DAL.Repositories;
using DAL;
using DAL.Repositories.Interfaces;

namespace MvcApplication1.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)kernel.Get(controllerType);
        }

        private void AddBindings()
        {
            kernel.Bind<IShopService>().To<ShopService>();
            kernel.Bind<IRepository<Produkty>>().To<BaseRepository<Produkty>>();
            kernel.Bind<IRepository<Dzialy>>().To<BaseRepository<Dzialy>>();
        }
    }
}