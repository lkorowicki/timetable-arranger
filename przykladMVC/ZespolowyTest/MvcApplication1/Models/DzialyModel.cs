﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;

namespace MvcApplication1.Models
{
    public class DzialyModel
    {
        public DzialyModel(IList<Dzialy> listaDzialow)
        {
            NazwyDzialow = new List<string>();
            foreach (Dzialy dzial in listaDzialow)
            {
                NazwyDzialow.Add(dzial.Nazwa);
            }
        }

        public List<string> NazwyDzialow { get; set; }
    }
}